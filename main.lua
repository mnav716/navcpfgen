program = {name = 'navcpfgen', version = 0.23, author = 'MNav'}

math.randomseed(os.time())

function gencpf()
  i = 1
  digito = {}

  while i <= 9 do
    digito[i] = math.random(0,9)
    i = i + 1
  end

  i = 1
  help = 10
  as = {}
  total = 0

  while i <= 9 do
    as[i] = help * digito[i]
    help = help - 1
    total = total + as[i]
    i = i + 1
  end

  resto = total % 11

  d = digito

    if resto < 2 then
      digito1 = 0
    else
      digito1 = 11 - resto
    end

--validado 1 digito

  d[10] = digito1

  i = 1
  help = 11
  as = {}
  total = 0

  while i <= 10 do
    as[i] = help * d[i]
    help = help - 1
    total = total + as[i]
    i = i + 1
  end

  resto = total % 11

  if resto < 2 then
    digito2 = 0
  else
    digito2 = 11 - resto
  end

  d[11] = digito2

--validado o segundo

  io.write(d[1] .. d[2] .. d[3] .. '.' ..
	   d[4] .. d[5] .. d[6] .. '.' ..
	   d[7] .. d[8] .. d[9] .. '-' ..
	   digito1 .. digito2 .. '\n')
end

function gen(nav)
  local i = 1
  while i <= nav do
    gencpf()
    i = i + 1
  end
end

io.write('sintaxe: gen <quantidade>\n')

while true do
  io.write('>>')
  cmd = io.read()
  cmd2, num = string.match(cmd, '(%a+)%s(%d+)')

  if cmd2 == 'gen' and num ~= nil then
    fnum = tonumber(num)
    gen(fnum)
  else
    print('erro de sintaxe')
  end

end